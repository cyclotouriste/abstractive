<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 */
?>

<div id="primary" class="widget-area main-column">

	<div id="sidebar-1">
		<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) { ?>
			<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-lila-lamp-150x194.jpg'; ?>" alt="Lila Lamp" itemprop="default-widget" width="150" height="194">
		<?php } ?>
	</div>

		<div id="sidebar-2">
		<?php if ( ! dynamic_sidebar( 'sidebar-2' ) ) { ?>
			<div class="widget-container widget_empty_div abstractive-empty-div" style="height: 250px"></div>
		<?php } ?>
	</div>

	<div id="sidebar-3">
		<?php if ( ! dynamic_sidebar( 'sidebar-3' ) ) { ?>
			<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-lila-lamp-150x194.jpg'; ?>" alt="Lila Lamp" itemprop="default-widget" width="150" height="194">
		<?php } ?>
	</div>

</div><!-- #primary .widget-area -->

