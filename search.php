<?php
/**
 * The template for displaying Search Results pages.
 *
 */

get_header(); ?>

	<div id="container">
		<div id="content" class="narrowcolumn main-column">

			<?php
			if (have_posts()) : ?>

				<h2 class="pagetitle"><?php _e( 'Search results', 'abstractive' ); ?></h2>

				<?php while (have_posts()) : the_post(); ?>
					<article>
					<div <?php post_class() ?>>
						<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( 'Permanent link to', 'abstractive' ); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
						<small><span class="posted-on"><?php
							the_date(); echo ', ';
							the_time(); ?></span>
							<?php abstractive_the_category();
							edit_post_link( __( 'Edit', 'abstractive' ), ' | ', ''); ?>
						</small>
					</div>
					</article>
				<?php endwhile; ?>

				<nav>
				<div class="navigation">
					<?php
					if ( function_exists('wp_pagenavi') ) {
						wp_pagenavi(); // nice navigation
					} else { ?>
						<div class="alignleft"><?php next_posts_link( __( '&laquo; Older posts', 'abstractive' ) ); ?></div>
						<div class="alignright"><?php previous_posts_link( __( 'Newer posts &raquo;', 'abstractive' ) ); ?></div>
					<?php } ?>
				</div>
				</nav>

			<?php
			else :

				_e( '<h2 class="center">No posts found.</h2>', 'abstractive' );

				get_search_form();

			endif; ?>

			<div class="clear"></div>

		</div><!-- #content -->

		<?php get_sidebar(); ?>

		<div class="clear"></div>

	</div><!-- #container -->

<?php get_footer(); ?>
