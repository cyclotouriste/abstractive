<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the <div id="wrapper"> and all content
 * after.
 *
 */
?>

	<footer>
	<div id="footer">

		<div id="footer_widgets">
			<div class="footer_widgets footer_widgets_1" id="footer_widgets_1">
				<?php if ( ! dynamic_sidebar( 'footer-column-1' ) ) { ?>
					<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-lamp-blue-368x400.jpg'; ?>" alt="Blue Lamp" itemprop="default-widget" width="368" height="400">
				<?php } ?>
				<div class="clear"></div>
			</div>
			<div class="footer_widgets footer_widgets_2" id="footer_widgets_2">
				<?php if ( ! dynamic_sidebar( 'footer-column-2' ) ) { ?>
					<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-lamp-green-368x400.jpg'; ?>" alt="Green Lamp" itemprop="default-widget" width="368" height="400">
				<?php } ?>
				<div class="clear"></div>
			</div>
			<div class="footer_widgets footer_widgets_3" id="footer_widgets_3">
				<?php if ( ! dynamic_sidebar( 'footer-column-3' ) ) { ?>
					<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-lamp-red-248x400.jpg'; ?>" alt="Red Lamp" itemprop="default-widget" width="248" height="400">
				<?php } ?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>

		<div id="colophon">
			&copy; <?php echo date("Y"); ?> <a href="<?php bloginfo('url'); ?>" title="Home"><?php bloginfo('name'); ?></a>
		</div>

	</div><!-- #footer -->
	</footer>

</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>
