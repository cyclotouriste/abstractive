

/*
 * JavaScript om een hover state (css class) aan te maken voor touchscreens.
 */
jQuery('.menu > li').click(function(){
	jQuery(this).toggleClass('hover');
});


/*
 * Sub-menu comes in sowly (not on mobile).
 */
jQuery('.menu > li').hover(function() {
	var mobile_click = jQuery('#site-navigation').hasClass('toggled');
	if ( mobile_click ) { return; }

	jQuery(this).closest('li').find('>ul').css({
		'opacity': 0,
	}).show().animate({
		'opacity': 1
	}, 400);
}, function() {
	var mobile_click = jQuery('#site-navigation').hasClass('toggled');
	if ( mobile_click ) { return; }

	jQuery(this).closest('li').find('>ul').fadeOut(200, function() {
		jQuery(this).hide();
	});
});


/*
 * Faux columns in jQuery.
 */
function equalHeight( group ) {
	var tallest = 0;
	var id = '';
	group.each( function() {
		jQuery(this).height('auto');
		var thisHeight = jQuery(this).height();
		if ( thisHeight > tallest ) {
			tallest = thisHeight;
			id = jQuery(this).attr('id');
		}
	});

	if ( id == 'primary' ) {
		jQuery( '#sidebar-3 .widget_sp_image img' ).css('border-bottom','0px');
		tallest = tallest - 5;
	}

	group.height( tallest );
}

function abstractive_equalHeight() {
	equalHeight( jQuery(".footer_widgets") );

	var wrapper_width = parseFloat( jQuery('#wrapper').width() );
	//console.log(wrapper_width);
	if ( wrapper_width < 900 ) {
		jQuery("#container > .main-column").height('auto');
		return;
	}

	equalHeight( jQuery("#container > .main-column") );
}

jQuery(document).ready(function() {
	abstractive_equalHeight();
	setTimeout( abstractive_equalHeight, 400 );
	setTimeout( abstractive_equalHeight, 1000 );
});
jQuery( window ).resize(function() {
	setTimeout( abstractive_equalHeight, 400 );
});
