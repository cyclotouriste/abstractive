<?php
/*
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments and the comment form.
 * The actual display of comments is handled by a callback to abstractive_comment which is located in the functions.php file.
 */
?>

<div id="comments">
	<?php
	if ( post_password_required() ) : ?>
			<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view it.', 'abstractive' ); ?></p>
		</div><!-- #comments -->
		<?php
		/* Stop the rest of comments.php from being processed,
		 * but don't kill the script entirely -- we still have to fully load the template.
		 */
		return;
	endif;


	if ( have_comments() ) : ?>
		<h3 id="comments-title">
			<p class="alignleft"><?php comments_number(__( 'No comments', 'abstractive' ), __( '1 comment', 'abstractive' ), __( '% comments', 'abstractive' )); ?></p>
			<p class="alignright"><a href="#respond" title="<?php _e( 'respond', 'abstractive' ); ?>"><?php _e( 'respond', 'abstractive' ); ?></a></p>
		</h3>

		<?php
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<!-- nice navigation -->
				<?php paginate_comments_links(); ?>
				<!-- standard navigation -->
				<!-- <div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older comments', 'abstractive' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer comments <span class="meta-nav">&rarr;</span>', 'abstractive' ) ); ?></div> -->
			</div>
			<?php
		endif; // check for comment navigation ?>

		<ol class="commentlist">
			<?php
			/* Loop through and list the comments. Tell wp_list_comments()
			 * to use abstractive_comment() to format the comments.
			 * See abstractive_comment() in functions.php for more.
			 */
			wp_list_comments( array( 'callback' => 'abstractive_comment' ) ); ?>
		</ol>

		<?php
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<!-- nice navigation -->
				<?php paginate_comments_links(); ?>
				<!-- standard navigation -->
				<!-- <div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older comments', 'abstractive' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer comments <span class="meta-nav">&rarr;</span>', 'abstractive' ) ); ?></div> -->
			</div> <!-- .navigation -->
			<?php
		endif; // check for comment navigation ?>

		<?php
		/* If there are no comments and comments are closed, let's leave a little note, shall we?
		 * But we only want the note on posts and pages that had comments in the first place.
		 */
		if ( ! comments_open() && get_comments_number() ) : ?>
			<p class="nocomments"><?php _e( 'Comments are closed.', 'abstractive' ); ?></p>
			<?php
		endif;

	endif; // end have_comments() ?>

	<?php
	comment_form(); ?>

</div><!-- #comments -->

