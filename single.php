<?php
/**
 * The Template for displaying all single posts.
 *
 */

get_header(); ?>

	<div id="container">
		<div id="content" class="narrowcolumn main-column">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article>
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h2><?php the_title(); ?></h2>
					<small><span class="posted-on"><?php
						the_date(); echo ', ';
						the_time(); ?></span>
						<?php abstractive_the_category(); echo ' | ';
						if ( comments_open() ) {
							// Comments are open
							_e( 'You can <a href="#respond">leave a comment</a>.', 'abstractive' );
						} elseif ( !comments_open() ) {
							esc_html_e( 'Comments are currently closed.', 'abstractive' );
						}
						edit_post_link( __( 'Edit', 'abstractive' ), ' | ', ''); ?>
					</small>

					<div class="entry">
						<?php
						if ( has_post_thumbnail() ) {
							$thumb_id = get_post_thumbnail_id(get_the_ID());
							$foto = wp_get_attachment_image_src( $thumb_id, 'full');
							$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
							?>
							<div class="post_thumbnail">
								<a href="<?php echo $foto[0]; ?>" title="<?php echo $alt; ?>" class="fancybox image" rel="lightbox">
									<?php the_post_thumbnail('medium'); ?>
								</a>
							</div><?php
						}

						the_content(); ?>
					</div>

					<?php comments_template(); ?>

				</div>
				</article>
				<?php
			endwhile; else:

				_e( '<h2 class="center">No posts found.</h2>', 'abstractive' );

			endif; ?>

			<div class="clear"></div>

		</div><!-- #content -->

		<?php get_sidebar(); ?>

		<div class="clear"></div>

	</div><!-- #container -->
<?php get_footer(); ?>
