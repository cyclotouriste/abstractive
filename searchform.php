<?php
/**
 * The template for displaying the standard searchform.
 *
 */

$search_query = get_search_query(); ?>

	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<label>
			<span class="screen-reader-text"><?php _e( 'Search for:', 'abstractive' ); ?></span>
			<input type="search" class="search-field" placeholder="<?php _e( 'Search...', 'abstractive' ); ?>" value="<?php echo $search_query; ?>" name="s" title="<?php _e( 'Search...', 'abstractive' ); ?>" />
		</label>
		<!-- De volgende regel kan gebruikt worden om alleen op posts of pagina's te zoeken -->
		<!-- <input type="hidden" value="post" name="post_type" id="post_type" /> -->
		<input type="submit" class="search-submit" value="<?php _e( 'Search', 'abstractive' ); ?>" />
	</form>
