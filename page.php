<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */

get_header(); ?>

	<div id="container">
		<div id="content" class="narrowcolumn main-column">
			<article>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="post" id="post-<?php the_ID(); ?>">
				<div class="entry">
					<?php the_content(); ?>

				</div> <!-- entry -->
			</div> <!-- post -->
			<?php endwhile; endif; ?>
			<?php edit_post_link( __( 'Edit', 'abstractive' ), '<p>', '</p>'); ?>
			</article>

			<div class="clear"></div>

		</div><!-- #content -->

		<?php get_sidebar(); ?>

		<div class="clear"></div>

	</div><!-- #container -->

<?php get_footer(); ?>
