<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 */

get_header(); ?>

	<div id="container">
		<div id="content" class="narrowcolumn main-column">

			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( 'Not Found.', 'abstractive' ); ?></h1>
				<div class="entry-content">
					<p><?php _e( 'Sorry, the page you are looking for does not exist. Maybe searching will help.', 'abstractive' ); ?></p>

					<?php get_search_form(); ?>

				</div><!-- .entry-content -->
			</div><!-- #post-0 -->

			<div class="clear"></div>

		</div><!-- #content -->

		<?php get_sidebar(); ?>

		<div class="clear"></div>

	</div><!-- #container -->

<?php get_footer(); ?>
