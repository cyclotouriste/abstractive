<?php
/*
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 */


function abstractive_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 */
	load_theme_textdomain( 'abstractive', get_template_directory() . '/languages' );


	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Let WordPress manage the document title. Since WP 4.1
	add_theme_support( 'title-tag' );

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Switches default core markup for search form, comment form, and comments
	// to output valid HTML5.
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 304,
		'height'      => 296,
		'flex-width'  => false,
	) );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'abstractive' ),
		'mobile'  => __( 'Mobile Menu', 'abstractive' )
	) );

	// Add thumbnail sizes for header
	add_image_size( 'top_thin_low_150_98', 150, 98, true ); // cropped
	add_image_size( 'top_thin_tall_150_194', 150, 194, true ); // cropped
	add_image_size( 'top_wide_low_376_98', 376, 98, true ); // cropped
	add_image_size( 'top_wide_tall_376_194', 376, 194, true ); // cropped

	// Add thumbnail sizes for sidebar
	add_image_size( 'sidebar_248_400', 248, 400, true ); // cropped
	add_image_size( 'footer_368_400', 368, 400, true ); // cropped


	// Crop everything.
	if ( ! get_option('small_crop') ) {
		add_option('small_crop', '1');
	} else {
		update_option('small_crop', '1');
	}

	if ( ! get_option('medium_crop') ) {
		add_option('medium_crop', '1');
	} else {
		update_option('medium_crop', '1');
	}

	if ( ! get_option('large_crop') ) {
		add_option('large_crop', '1');
	} else {
		update_option('large_crop', '1');
	}

}
add_action( 'after_setup_theme', 'abstractive_setup' );


/*
 * Add custom sizes to dropdown chooser.
 */
function abstractive_add_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
		'top_thin_low_150_98'   => __( 'Top thin low', 'abstractive' ),
		'top_thin_tall_150_194' => __( 'Top thin high', 'abstractive' ),
		'top_wide_low_376_98'   => __( 'Top wide low', 'abstractive' ),
		'top_wide_tall_376_194' => __( 'Top wide high', 'abstractive' ),
		'sidebar_248_400'       => __( 'Sidebar fit', 'abstractive' ),
		'footer_368_400'        => __( 'Footer fit', 'abstractive' ),
    ) );
}
add_filter( 'image_size_names_choose', 'abstractive_add_custom_sizes' );


/*
 * Enqueue scripts and styles.
 */
function abstractive_scripts() {
	wp_enqueue_style( 'abstractive-style', get_stylesheet_uri(), false, '1.0', 'all' );

	wp_enqueue_script( 'abstractive-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );
	wp_enqueue_script( 'abstractive-frontend', get_template_directory_uri() . '/js/frontend.js', array(), '1.0', true );

	wp_enqueue_script( 'jquery' );

	wp_dequeue_style( 'open-sans' );
	wp_enqueue_style( 'open-sans-abstractive', get_template_directory_uri() . '/fonts/open-sans.css', false, '1.4.2', 'all' );
}
add_action( 'wp_enqueue_scripts', 'abstractive_scripts' );


/*
 *  Register and Enqueue local Open Sans
 */
function abstractive_admin_scripts() {
	wp_dequeue_style( 'open-sans' );
	wp_enqueue_style( 'open-sans-abstractive', get_template_directory_uri() . '/fonts/open-sans.css', false, '1.4.2', 'all' );

	wp_register_style( 'abstractive-admin', get_template_directory_uri() . '/admin-css/admin.css', false, '1.0', 'all' );
	wp_enqueue_style( 'abstractive-admin' );
}
add_action( 'admin_enqueue_scripts', 'abstractive_admin_scripts' );


/*
 * Sets the post excerpt length to 40 characters.
 */
function abstractive_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'abstractive_excerpt_length' );


/*
 * Returns a "Continue Reading" link for excerpts
 */
function abstractive_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Read more...', 'abstractive' ) . '</a>';
}


/*
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and abstractive_continue_reading_link().
 */
function abstractive_auto_excerpt_more( $more ) {
	return ' &hellip; ' . abstractive_continue_reading_link();
}
add_filter( 'excerpt_more', 'abstractive_auto_excerpt_more' );


/*
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 */
function abstractive_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= abstractive_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'abstractive_custom_excerpt_more' );


/*
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function abstractive_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'abstractive_page_menu_args' );


/*
 * Show our favicon, if none is set.
 */
function abstractive_site_icon_meta_tags() {
	if ( has_site_icon() || is_customize_preview() ) {
		return;
	}

	$path = get_stylesheet_directory_uri() . '/images'; // URI file, support childthemes.

	$meta_tags = array();
	$meta_tags[] = sprintf( '<link rel="icon" href="%s" sizes="32x32" />', esc_url( $path . '/favicon-32.png' ) );
	$meta_tags[] = sprintf( '<link rel="icon" href="%s" sizes="192x192" />', esc_url( $path . '/favicon-192.png' ) );
	$meta_tags[] = sprintf( '<link rel="apple-touch-icon-precomposed" href="%s" />', esc_url( $path . '/favicon-180.png' ) );
	$meta_tags[] = sprintf( '<meta name="msapplication-TileImage" content="%s" />', esc_url( $path . '/favicon-270.png' ) );

	$meta_tags = apply_filters( 'site_icon_meta_tags', $meta_tags );
	$meta_tags = array_filter( $meta_tags );

	foreach ( $meta_tags as $meta_tag ) {
		echo "$meta_tag\n";
	}
}
add_filter( 'wp_head', 'abstractive_site_icon_meta_tags' );
add_filter( 'admin_head', 'abstractive_site_icon_meta_tags' );


/*
 * Template for comments and pingbacks.
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function abstractive_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
			<div class="comment-author vcard">
				<?php echo get_avatar( $comment, 40 ); ?>
				<?php printf( __( '%s <span class="says">writes:</span>', 'abstractive' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
			</div><!-- .comment-author .vcard -->
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Your comment awaits moderation.', 'abstractive' ); ?></em>
				<br />
			<?php endif; ?>

			<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
				<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'abstractive' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'abstractive' ), ' ' );
				?>
			</div><!-- .comment-meta .commentmetadata -->

			<div class="comment-body"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'abstractive' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'abstractive' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}


/*
 * Register widgetized areas.
 */
function abstractive_widgets_init() {

	// Header Widget Area's
	register_sidebar( array(
			'name' => __( 'Top 1 up', 'abstractive' ),
			'id' => 'top-1-up',
			'description' => __( 'Top 1 up', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );
	register_sidebar( array(
			'name' => __( 'Top 1 under', 'abstractive' ),
			'id' => 'top-1-under',
			'description' => __( 'Top 1 under', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );
	register_sidebar( array(
			'name' => __( 'Top 2 up', 'abstractive' ),
			'id' => 'top-2-up',
			'description' => __( 'Top 2 up', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );
	register_sidebar( array(
			'name' => __( 'Top 2 under', 'abstractive' ),
			'id' => 'top-2-under',
			'description' => __( 'Top 2 under', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );
	register_sidebar( array(
			'name' => __( 'Top 3 up', 'abstractive' ),
			'id' => 'top-3-up',
			'description' => __( 'Top 3 up', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );
	register_sidebar( array(
			'name' => __( 'Top 3 under', 'abstractive' ),
			'id' => 'top-3-under',
			'description' => __( 'Top 3 under', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );

	// Sidebar.
	register_sidebar( array(
		'name' => __( 'Sidebar 1', 'abstractive' ),
		'id' => 'sidebar-1',
		'description' => __( 'Sidebar 1', 'abstractive' ),
		'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Sidebar 2', 'abstractive' ),
		'id' => 'sidebar-2',
		'description' => __( 'Sidebar 2', 'abstractive' ),
		'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Sidebar 3', 'abstractive' ),
		'id' => 'sidebar-3',
		'description' => __( 'Sidebar 3', 'abstractive' ),
		'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Footer Widget Area's
	register_sidebar( array(
			'name' => __( 'Footer Column 1', 'abstractive' ),
			'id' => 'footer-column-1',
			'description' => __( 'The Footer Column 1', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );
	register_sidebar( array(
			'name' => __( 'Footer Column 2', 'abstractive' ),
			'id' => 'footer-column-2',
			'description' => __( 'The Footer Column 2', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );
	register_sidebar( array(
			'name' => __( 'Footer Column 3', 'abstractive' ),
			'id' => 'footer-column-3',
			'description' => __( 'The Footer Column 3', 'abstractive' ),
			'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div></section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
	) );

}
add_action( 'widgets_init', 'abstractive_widgets_init' );


function abstractive_headers() {
	// Always use the latest IE mode. Needs to be tested still...
	header("X-UA-Compatible: IE=Edge");
}
add_action( 'after_setup_theme', 'abstractive_headers' );


/*
 * Template function to display categories for a post.
 */
function abstractive_the_category() {
	$categories = get_the_category();
	if ( is_array( $categories ) && ! empty( $categories ) ) {
		echo ' | ';
		esc_html_e( 'Placed in ', 'abstractive' );
		the_category(', ');
	}
}


include('includes/class-wp-widget-div.php');
