=== Abstractive ===
Contributors: mpol
Requires at least: WordPress 4.5
Tested up to: 4.9
Version: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, right-sidebar, accessibility-ready, custom-logo, editor-style, featured-images, rtl-language-support, sticky-post, threaded-comments, translation-ready, artsy, blog, photography


== Description ==
Abstractive is an abstract theme inspired by the art of Piet Mondriaan. It features image widgets and a right sidebar that works perfectly for blogs and websites.
It has a black and white color scheme. It has a fixed width for desktop and separate menus for desktop and mobile. Abstractive will make your WordPress look beautiful everywhere.

* Abstract layout
* Fixed Width
* Responsive layout for Mobile
* Separate menus for Desktop and Mobile
* Custom Logo
* (Image) Widgets in Header, Sidebar and Footer
* WooCommerce compatible
* GPL license.


== Custom Setup ==

= Logo install =

You can go to Appearance > Customizer > Site Identity and select a file for the custom logo.
It will be cut to a max width of 304px and height of 296px to fit the theme.

= Favicon install =

You can go to Appearance > Customizer > Site Identity and select a file for the favicon.
It needs to be a file with a width of 512px and height of 512px.

= Image Widgets =

The theme and look of the website very much depend on good images. If you haven't added any image widgets yet, you will see some default images at the location of the widget areas.
You are suggested to add image widgets to those widget areas.
Please take care to select the right image thumbnail sizes.

= Text Widgets =

A text widget in the header wil get a custom background and custom text color.
By default there is one text widget in the header with formatted text like this:

	<span style="color:#333">Phone: 06-12345678</span><br />
	Address: Brink Road 1, Nottingham<br />
	Email: abstractive@example.com

You are suggested to style it in a similar fashion.

= Sidebar and Footer Widgets =

In the sidebar and footer you can add image widgets, or any other widget you might prefer.

In case you need some empty space somewhere between your widgets, there is a custom widget Empty Div, which can do that.


== Support and Translations ==

= Support =

If you have a problem or a feature request, please post it on the theme's support forum on [wordpress.org](https://wordpress.org/support/theme/abstractive). I will do my best to respond as soon as possible.

If you send me an email, I will not reply. Please use the support forum.

= Translations =

Translations can be added very easily through [GlotPress](https://translate.wordpress.org/projects/wp-themes/abstractive).
You can start translating strings there for your locale. They need to be validated though, so if there's no validator yet, and you want to apply for being validator (PTE), please post it on the support forum.
I will make a request on make/polyglots to have you added as validator for this theme/locale.


== FAQ ==

= I have existing images that are not in the right size for the widgets =

For the image widgets you can best use the sizes that are required for those widgets. If you have already existing images in your media library, you want to generate thumbnails in those correct sizes.
You can use a plugin called [Regenerate Thumbnails](https://wordpress.org/plugins/regenerate-thumbnails/) to generate all thumbnails.

= I want to use a Child Theme =

If you want to make more customizations than what is available by default, a Child Theme is the way to go.
Please take a look at [The Codex page on Child Themes](https://codex.wordpress.org/Child_Themes)
You also might want to look at the plugin [Orbisius](https://wordpress.org/plugins/orbisius-child-theme-creator/)


== Changelog ==

= 1.0.0 =
* 2018-11-16
* Initial release
