<?php

/*
	Copyright  2016 - 2018  Marcel Pol  (email: marcel@timelord.nl)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


/**
 * Core class used to implement a widget.
 *
 * @since 2.8.0
 *
 * @see WP_Widget
 */
class WP_Widget_Div extends WP_Widget {

	/**
	 * Sets up a new Widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_empty_div',
			'description' => __( 'Empty Div.', 'abstractive' ),
			'customize_selective_refresh' => true,
		);
		$control_ops = array( 'width' => 400, 'height' => 350 );
		parent::__construct( 'lege_div', __( 'Empty Div', 'abstractive' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$height = empty( $instance['height'] ) ? '200' : $instance['height'];

		echo $args['before_widget'];

		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>


		<div class="abstractive-empty-div" style="height: <?php echo $height;?>px"></div>

		<div class="clear"></div> <?php

		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['height'] = (int) $new_instance['height'];
		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'height' => '200' ) );
		$height = (int) $instance['height'];
		?>

		<select name="<?php echo esc_attr( $this->get_field_name( 'sortby' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'sortby' ) ); ?>" class="widefat">
			<option value="200"<?php selected( $instance['height'], '200' ); ?>><?php _e( '200 pixels', 'abstractive' ); ?></option>
			<option value="250"<?php selected( $instance['height'], '250' ); ?>><?php _e( '250 pixels', 'abstractive' ); ?></option>
			<option value="300"<?php selected( $instance['height'], '300' ); ?>><?php _e( '300 pixels', 'abstractive' ); ?></option>
			<option value="350"<?php selected( $instance['height'], '350' ); ?>><?php _e( '350 pixels', 'abstractive' ); ?></option>
			<option value="400"<?php selected( $instance['height'], '400' ); ?>><?php _e( '400 pixels', 'abstractive' ); ?></option>
		</select>

		<?php
	}
}


function abstractive_wp_widget_div_init() {
	register_widget('WP_Widget_Div');
}
add_action('widgets_init', 'abstractive_wp_widget_div_init' );
