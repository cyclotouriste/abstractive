<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="wrapper" and "header">
 *
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php
/* Always have wp_head() just before the closing </head>
 * tag of your theme, or you will break many plugins, which
 * generally use this hook to add elements to <head> such
 * as styles, scripts, and meta tags.
 */
wp_head();
?>

</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<header>
	<div id="header">
		<div class="clear"></div>
		<div id="bovenvak">

			<div id="logo">
				<?php
				$logo = get_custom_logo(); // WP 4.5
				if ( $logo ) {
					echo $logo;
				} else {
					$file = get_template_directory_uri() . '/images/logo.png'; // Only parent theme.
					?>
					<a href="<?php bloginfo('url'); ?>" title="Home">
						<span class="hidden"><?php bloginfo('blogname'); ?></span>
						<img class="custom-logo" src="<?php echo $file; ?>" alt="Abstractive Logo" itemprop="logo" width="304" height="296">
					</a>
					<?php
				}
				?>
				</a>
			</div>

			<div id="bovenvak_1">
				<div id="bovenvak_1_boven">
					<?php if ( ! dynamic_sidebar( 'top-1-up' ) ) { ?>
						<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-lila-lamp-150x194.jpg'; ?>" alt="Lila Lamp" itemprop="default-widget" width="150" height="194">
					<?php } ?>
				</div>
				<div id="bovenvak_1_onder">
					<?php if ( ! dynamic_sidebar( 'top-1-under' ) ) { ?>
						<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-enamel-cookware-150x98.jpg'; ?>" alt="Enamel Cookware" itemprop="default-widget" width="150" height="98">
					<?php } ?>
				</div>
			</div>

			<div id="bovenvak_2">
				<div id="bovenvak_2_boven">
					<?php if ( ! dynamic_sidebar( 'top-2-up' ) ) { ?>
						<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-magical-lamp-150x98.jpg'; ?>" alt="Magical Lamp" itemprop="default-widget" width="150" height="98">
					<?php } ?>
				</div>
				<div id="bovenvak_2_onder">
					<?php if ( ! dynamic_sidebar( 'top-2-under' ) ) { ?>
						<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-turning-chair-150x194.jpg'; ?>" alt="Turning Chair" itemprop="default-widget" width="150" height="194">
					<?php } ?>
				</div>
			</div>

			<div id="bovenvak_3">
				<div id="bovenvak_3_boven">
					<?php if ( ! dynamic_sidebar( 'top-3-up' ) ) { ?>
						<img class="default-widget" src="<?php echo get_template_directory_uri() . '/images/widget-sports-car-376x194.jpg'; ?>" alt="Sports Car" itemprop="default-widget" width="376" height="194">
					<?php } ?>
				</div>
				<div id="bovenvak_3_onder">
					<?php if ( ! dynamic_sidebar( 'top-3-under' ) ) { ?>
						<div class="widget_custom_html widget-container textwidget custom-html-widget">
							<span style="color:#333">Phone: 06-12345678</span><br />
							Address: Brink Road 1, Nottingham<br />
							Email: abstractive@example.com
						</div>
					<?php } ?>
				</div>
			</div>

		</div>

		<nav id="site-navigation" class="main-navigation" role="navigation">
		<div id="access">
			<?php /* Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
			<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'abstractive' ); ?>"><?php _e( 'Skip to content', 'abstractive' ); ?></a></div>

			<!-- Toggle button -->
			<button class="menu-toggle" aria-controls="mobile-menu" aria-expanded="false"><?php esc_html_e('Main menu', 'abstractive'); ?></button>

			<?php
			// Our navigation menu. If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used. ?>

			<div id="main-nav"><?php
				wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) );
			?></div>

			<div id="mobile-nav"><?php
				wp_nav_menu( array( 'container_class' => 'menu-mobile', 'theme_location' => 'mobile' ) );
			?></div>

			<div class="clear"></div>
		</div><!-- #access -->
		</nav>

	</div><!-- #header -->
	</header>
